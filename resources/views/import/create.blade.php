@if (Session::has('success'))
    <div class="alert alert-success">
        <p>{{ Session::get('success') }}</p>
    </div>
    <br>
@endif

<form method="post" action="{{ url('import/store') }}" enctype="multipart/form-data">
    @csrf
    <input type="file" name="file_import">
    <br>
    {{ $errors->first('file_import') }}
    <br>
    <button>Импортировать</button>
</form>
<form method="post" action="{{ url('import/destroy') }}" enctype="multipart/form-data">
    @csrf
    <button>Очистить таблицу транзакций</button>
</form>