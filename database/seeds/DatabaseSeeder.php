<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        $user = new User([
            'name'     => 'admin',
            'email'    => 'admin@localhost',
            'password' => Hash::make('123456'),
        ]);
        $user->save();
    }
}
