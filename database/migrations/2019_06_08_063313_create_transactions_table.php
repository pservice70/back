<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('card_id')->unsigned()->index();
            $table->foreign('card_id')
                ->references('id')
                ->on('cards')
                ->onDelete('cascade');
            $table->integer('terminal_id')->unsigned()->index();
            $table->foreign('terminal_id')
                ->references('id')
                ->on('terminals')
                ->onDelete('cascade');
            $table->integer('stop_id')->unsigned()->index()->nullable();
            $table->foreign('stop_id')
                ->references('id')
                ->on('stops')
                ->onDelete('cascade');
            $table->string('cost');
            $table->string('longitude');
            $table->string('latitude');
            $table->dateTime('time');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
