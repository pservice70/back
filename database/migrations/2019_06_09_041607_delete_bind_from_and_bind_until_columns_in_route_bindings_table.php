<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class DeleteBindFromAndBindUntilColumnsInRouteBindingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('route_bindings', function (Blueprint $table) {
            $table->dropColumn('bind_from');
            $table->dropColumn('bind_until');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('route_bindings', function (Blueprint $table) {
            //
        });
    }
}
