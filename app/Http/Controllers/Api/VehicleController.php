<?php

namespace App\Http\Controllers\Api;

use App\Vehicle;
use App\VehicleType;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VehicleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data' => Vehicle::with(['type', 'terminals', 'routes'])->get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'data' =>
                [
                    'vehicle_types' => VehicleType::all()->toArray(),
                ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Vehicle::insert([
            'type_id' => $request->get('type_id'),
            'number' => $request->get('number'),
        ]);

        return response()->json(['data' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicle = Vehicle::findOrFail($id);
        $vehicle->load(['terminals', 'routes']);
        return response()->json(['data' => $vehicle]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        /** @var Vehicle $model */
        $model = Vehicle::findOrFail($id);
        $model->update([
            'type_id' => $request->get('type_id'),
            'number' => $request->get('number'),
        ]);

        if ($request->has('terminal_ids')) {
            $terminalIds = $request['terminal_ids'];
            if (!empty($terminalIds)) {
                $terminalIds = explode(',', $terminalIds);
                $model->terminals()->sync($terminalIds);
            } else {
                $model->terminals()->sync([]);
            }
        }

        if ($request->has('route_ids')) {
            $routeIds = $request['route_ids'];
            if (!empty($routeIds)) {
                $routeIds = explode(',', $routeIds);
                $model->routes()->sync($routeIds);
            } else {
                $model->routes()->sync([]);
            }
        }


        return response()->json(['data' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Vehicle::destroy($id);

        return response()->json(['data' => true]);
    }
}
