<?php

namespace App\Http\Controllers\Api;

use App\Route;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RouteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data' => Route::all()->toArray()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Route::insert([
            'name' => $request->get('name'),
            'description' => $request->get('longitude'),
        ]);

        return response()->json(['data' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['data' => Route::findOrFail($id)->toArray()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json(['data' => Route::findOrFail($id)->toArray()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Route::findOrFail($id);
        $model->update([
            'name' => $request->get('name'),
            'description' => $request->get('longitude'),
        ]);

        return response()->json(['data' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Route::destroy($id);

        return response()->json(['data' => true]);
    }

    public function importStops(Route $route, Request $request)
    {
        $route->stops()->delete();
        $data = $request['data'];
        $rows = explode("\n", $data);
        $sort = 1;
        foreach ($rows as $row) {
            $fields = explode("\t", $row);

            $route->stops()->attach($fields[0], [
                'sort' => $sort++,]);
        }
    }

    public function stops(Route $route)
    {
        return response()->json(['data'=>$route->stops]);
    }
}
