<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class StatController extends Controller
{
    public function getTransactionsSumDate(Request $request)
    {
        $date_from = $request->get('date_from', date('Y-m-d', time()-3600*24*90));
        $date_to = $request->get('date_to', date('Y-m-d', time()));
        $date_to = substr(date('Y-m-d', strtotime($date_to.'1 day')),0,10);

        $results = DB::table('transactions as t')
            ->select([
                DB::raw('date(t.time) as transaction_date'),
                DB::raw('count(*) as transaction_count'),
                DB::raw('sum(t.cost) as transaction_cost')
            ])
            ->where([['time', '>', $date_from],['time', '<', $date_to]])
            ->groupBy('transaction_date')
            ->orderBy('transaction_date')
            ->get();

        return $results;
    }

    public function getTransactionsByStops(Request $request)
    {
        $date_from = $request->get('date_from', date('Y-m-d H:i:s', time()-3600*24*90));
        $date_to = $request->get('date_to', date('Y-m-d H:i:s', time()));

        $results = DB::table('transactions as t')
            ->join('stops as s', 's.id','=','t.stop_id')
            ->select(['s.id','s.name','s.longitude','s.latitude',
                DB::raw('count(*) as transaction_count')
            ])
            ->where([['time', '>', $date_from],['time', '<', $date_to]])
            ->groupBy(['s.id','s.name','s.longitude','s.latitude'])
            ->orderBy('s.id')
            ->get();

        return $results;
    }

    public function getTransactionsCount(Request $request)
    {
        $date_from = $request->get('date_from', date('Y-m-d H:i:s', time()-3600*24*90));
        $date_to = $request->get('date_to', date('Y-m-d H:i:s', time()));

        $counts = DB::table('transactions as t')
            ->select(['card_id', 'cost', 'cards.gender'])
            ->join('cards','card_id', '=', 'cards.id')
            ->where([['time', '>', $date_from],['time', '<', $date_to]])
            ->get();

        $routes = DB::table('vehicles as v')
            ->join('route_bindings as rb', 'v.id', '=', 'rb.vehicle_id')
            ->join('routes as r', 'rb.route_id', '=', 'r.id')
            ->join('vehicle_terminals as vt', 'v.id', '=', 'vt.vehicle_id')
            ->join('transactions as t', 'vt.terminal_id', '=', 't.terminal_id')
            ->select([
                'rb.route_id',
                'r.name',
                DB::raw('count(*) as transaction_count')
            ])
            ->where([['time', '>', $date_from],['time', '<', $date_to]])
            ->groupBy(['route_id', 'name'])
            ->get();

        return [
            'count' => $counts->count(),
            'female' => $counts->where('gender', 0)->count(),
            'male' => $counts->where('gender', 1)->count(),
            'routes' => $routes
        ];
    }

    public function getTransactionsSumHour(Request $request)
    {
        $date = $request->get('date', date('Y-m-d', time()));

        $results = DB::table('transactions as t')
            ->select([
                DB::raw('hour(t.time) as transaction_time'),
                DB::raw('count(*) as transaction_count'),
                DB::raw('sum(t.cost) as transaction_cost')
            ])
            ->where([['t.time', '>', $date.' 00:00:00'], ['t.time', '<', $date.' 23:59:59']])
            ->groupBy('transaction_time')
            ->orderBy('transaction_time')
            ->get();

        return $results;
    }

    public function getRoutesSumDate(Request $request)
    {
        $date_from = $request->get('date_from', date('Y-m-d H:i:s', time()-3600*24*90));
        $date_to = $request->get('date_to', date('Y-m-d H:i:s', time()));

        $results = DB::table('vehicles as v')
            ->join('route_bindings as rb', 'v.id', '=', 'rb.vehicle_id')
            ->join('routes as r', 'rb.route_id', '=', 'r.id')
            ->join('vehicle_terminals as vt', 'v.id', '=', 'vt.vehicle_id')
            ->join('transactions as t', 'vt.terminal_id', '=', 't.terminal_id')
            ->select([
                'rb.route_id',
                'r.name',
                DB::raw('date(t.time) as transaction_date'),
                DB::raw('sum(t.cost) as transaction_cost')
            ])
            ->where([['time', '>', $date_from],['time', '<', $date_to]])
            ->groupBy(['route_id', 'name', 'transaction_date'])
            ->orderBy('transaction_date')
            ->get();

        return $results;
    }
}
