<?php

namespace App\Http\Controllers\Api;

use App\Transaction;
use App\Card;
use App\Terminal;
use App\Stop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return ['data' => Transaction::query()->orderBy('id', 'desc')->limit(1000)->get()];
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'data' =>
                [
                    'cards' => Card::all()->toArray(),
                    'terminals' => Terminal::all()->toArray(),
                    'stops' => Stop::all()->toArray(),
                ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Transaction::insert([
            'card_id' => $request->get('card_id'),
            'terminal_id' => $request->get('terminal_id'),
            'stop_id' => $request->get('stop_id'),
            'cost' => $request->get('cost'),
            'longitude' => $request->get('longitude'),
            'latitude' => $request->get('latitude'),
            'time' => $request->get('time'),
        ]);

        return response()->json(['data' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['data' => Transaction::findOrFail($id)->toArray()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json(['data' => Transaction::findOrFail($id)->toArray()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Transaction::findOrFail($id);
        $model->update([
            'card_id' => $request->get('card_id'),
            'terminal_id' => $request->get('terminal_id'),
            'stop_id' => $request->get('stop_id'),
            'cost' => $request->get('cost'),
            'longitude' => $request->get('longitude'),
            'latitude' => $request->get('latitude'),
            'time' => $request->get('time'),
        ]);

        return response()->json(['data' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Transaction::destroy($id);

        return response()->json(['data' => true]);
    }

    public function import(Request $request)
    {
        $data = $request['data'];
        $rows = explode("\n", $data);
        foreach ($rows as $row) {
            $fields = explode("\t", $row);
            Transaction::insert([
                'card_id' => $fields[0],
                'terminal_id' => $fields[1],
                'stop_id' => $fields[2],
                'cost' => $fields[3],
                'time' => $fields[4],
            ]);
        }
    }

    public function destroyAll()
    {
        Transaction::truncate();
    }
}
