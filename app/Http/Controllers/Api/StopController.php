<?php

namespace App\Http\Controllers\Api;

use App\Stop;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data' => Stop::all()->toArray()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Stop::insert([
            'name' => $request->get('name'),
            'longitude' => $request->get('longitude'),
            'latitude' => $request->get('latitude'),
        ]);

        return response()->json(['data' => true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['data' => Stop::findOrFail($id)->toArray()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json(['data' => Stop::findOrFail($id)->toArray()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = Stop::findOrFail($id);
        $model->update([
            'name' => $request->get('name'),
            'longitude' => $request->get('longitude'),
            'latitude' => $request->get('latitude'),
        ]);

        return response()->json(['data' => true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Stop::destroy($id);

        return response()->json(['data' => true]);
    }

    public function import(Request $request)
    {
        Stop::query()->delete();
        $data = $request['data'];
        $rows = explode("\n", $data);
        foreach ($rows as $row) {
            $fields = explode("\t", $row);
            Stop::insert([
                'id' => $fields[0],
                'name' => $fields[1],
                'longitude' => $fields[2],
                'latitude' => $fields[3],]);
        }
    }
}
