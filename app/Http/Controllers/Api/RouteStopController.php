<?php

namespace App\Http\Controllers\Api;

use App\Route;
use App\Stop;
use App\RouteStop;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RouteStopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data'=>RouteStop::all()->toArray()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'data'=>
                [
                    'routes' => Route::all()->toArray(),
                    'vehicle'=> Stop::all()->toArray(),
                ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        RouteStop::insert([
            'stop_id'  => $request->get('stop_id'),
            'route_id' => $request->get('route_id'),
            'sort'     => $request->get('sort'),
        ]);

        return response()->json(['data'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['data'=>RouteStop::findOrFail($id)->toArray()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json(['data'=>RouteStop::findOrFail($id)->toArray()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = RouteStop::findOrFail($id);
        $model->update([
            'stop_id'  => $request->get('stop_id'),
            'route_id' => $request->get('route_id'),
            'sort'     => $request->get('sort'),
        ]);

        return response()->json(['data'=>true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RouteStop::destroy($id);

        return response()->json(['data'=>true]);
    }
}
