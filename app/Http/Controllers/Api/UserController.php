<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

/**
 * @package App\Http\Controllers
 */
class UserController extends Controller
{
    /**
     * @param Request $request
     *
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (Auth::attempt($credentials)) {
            $user = Auth::user();
            $success['token'] = $user->createToken('PService')->accessToken;
            $success['user'] = $user;
            return response()->json(['success'=>$success], 200);
        } else {
            return response()->json(['error' => 'UnAuthorised'], 401);
        }
    }
}
