<?php

namespace App\Http\Controllers\Api;

use App\Route;
use App\RouteBinding;
use App\Vehicle;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RouteBindingController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return response()->json(['data'=>RouteBinding::all()->toArray()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json([
            'data'=>
            [
                'routes' => Route::all()->toArray(),
                'vehicle'=> Vehicle::all()->toArray(),
            ]
        ]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        RouteBinding::insert([
            'vehicle_id' => $request->get('vehicle_id'),
            'route_id'   => $request->get('route_id'),
            'bind_from'  => $request->get('bind_from'),
            'bind_until' => $request->get('bind_until'),
        ]);

        return response()->json(['data'=>true]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return response()->json(['data'=>RouteBinding::findOrFail($id)->toArray()]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return response()->json(['data'=>RouteBinding::findOrFail($id)->toArray()]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $model = RouteBinding::findOrFail($id);
        $model->update([
            'vehicle_id' => $request->get('vehicle_id'),
            'route_id'   => $request->get('route_id'),
            'bind_from'  => $request->get('bind_from'),
            'bind_until' => $request->get('bind_until'),
        ]);

        return response()->json(['data'=>true]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        RouteBinding::destroy($id);

        return response()->json(['data'=>true]);
    }
}
