<?php

namespace App\Http\Controllers;

use App\Transaction;
use Illuminate\Http\Request;

class ImportController extends Controller
{
    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('import.create');
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(Request $request)
    {
        $request->validate([
            'file_import' => 'required'
        ]);

        $file = $request->file('file_import');
        $file->move('temp', $file->getClientOriginalName());
        $lines = file('temp/'.$file->getClientOriginalName());
        foreach($lines as $data)
        {
            $data = explode(',',$data);

            Transaction::insert([
                'card_id' => $data[0],
                'terminal_id' => $data[1],
                'stop_id' => $data[2],
                'cost' => $data[3],
                'time' => $data[4],
            ]);
        }

        return back()->with('success', 'Insert Record successfully.');
    }

    public function destroy()
    {
        Transaction::truncate();
        return back()->with('success', 'Очищено.');
    }
}
