<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Stop extends Model
{
    protected $guarded = [];

    public function routes()
    {
        return $this->belongsToMany('App\Route', 'route_stops', 'stop_id', 'route_id');
    }
}
