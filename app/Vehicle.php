<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;

class Vehicle extends Model
{
    protected $guarded = [];

    public function type() : BelongsTo
    {
        return $this->belongsTo(VehicleType::class);
    }

    public function terminals() : BelongsToMany
    {
        return $this->belongsToMany(Terminal::class,'vehicle_terminals');
    }

    public function routes() : BelongsToMany
    {
        return $this->belongsToMany(Route::class,'route_bindings');
    }
}
