<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Route extends Model
{
    protected $guarded = [];

    public function stops()
    {
        return $this->belongsToMany('App\Stop', 'route_stops', 'route_id', 'stop_id');
    }
}
