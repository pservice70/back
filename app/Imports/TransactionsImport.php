<?php
namespace App\Imports;

use App\Transaction;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Concerns\ToModel;

class TransactionsImport implements ToModel
{
    /**
     * @param array $row
     * @return Transaction|\Illuminate\Database\Eloquent\Model|\Illuminate\Database\Eloquent\Model[]|null
     */
    public function model(array $row)
    {
        // Здесь можно модифицировать сырые данные из файла перед заполнением модели
        return new Transaction([
            'card_id'     => $row[0],
            'terminal_id'    => $row[1],
            'stop_id'    => $row[2],
            'cost'    => $row[3],
            'longitude'    => $row[4],
            'latitude'    => $row[5],
            'time'    => $row[6],
        ]);
    }


}