<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('/login', 'Api\UserController@login');
//['middleware' => 'auth:api']
Route::group(['middleware' => 'auth:api'], function () {
    Route::get('/state', function () {
        return ['user' => Auth::user()];
    });
    Route::post('/stops/import', 'Api\StopController@import');
    Route::post('/cards/import', 'Api\CardController@import');
    Route::post('/transactions/import', 'Api\TransactionController@import');
    Route::post('/transactions/destroyAll', 'Api\TransactionController@destroyAll');
    Route::post('/routes/{route}/import_stops', 'Api\RouteController@importStops');
    Route::get('/routes/{route}/stops', 'Api\RouteController@stops');

    Route::prefix('stat')->group(function () {
        Route::post('/transactions/sum-date', 'Api\StatController@getTransactionsSumDate');
        Route::post('/transactions/by-stops', 'Api\StatController@getTransactionsByStops');
        Route::post('/transactions/by-hours', 'Api\StatController@getTransactionsSumDate');
        Route::post('/transactions/sum-hour', 'Api\StatController@getTransactionsSumHour');
        Route::post('/routes/sum-date', 'Api\StatController@getRoutesSumDate');
        Route::post('/routes/transactions-count', 'Api\StatController@getTransactionsCount');
    });

    Route::resource('stops', 'Api\StopController', ['except' => ['create']]);
    Route::resource('cards', 'Api\CardController', ['except' => ['create']]);
    Route::resource('route-bindings', 'Api\RouteBindingController');
    Route::resource('routes', 'Api\RouteController', ['except' => ['create']]);
    Route::resource('route-stops', 'Api\RouteStopController');
    Route::resource('terminals', 'Api\TerminalController', ['except' => ['create']]);
    Route::resource('transactions', 'Api\TransactionController');
    Route::resource('vehicles', 'Api\VehicleController');
    Route::resource('vehicle-types', 'Api\VehicleTypeController', ['except' => ['create']]);
});




